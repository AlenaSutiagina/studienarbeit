import re

CLEANR = re.compile('<.*?>')
CLEANA = re.compile('<a.*/a>')

def cleanhtml(raw_html):
  nolinks = re.sub(CLEANA, '', raw_html)
  cleantext = re.sub(CLEANR, '', nolinks)
  return cleantext

text = open("D:/Studienarbeit/textNoTags.txt", "w+", encoding='utf-8')
with open("D:/Studienarbeit/text.txt", encoding='utf-8') as f:
    for line in f:
        c = cleanhtml(line)
        text.write(c)